CREATE TABLE IF NOT EXISTS "schema_migrations" ("version" varchar NOT NULL PRIMARY KEY);
CREATE TABLE IF NOT EXISTS "ar_internal_metadata" ("key" varchar NOT NULL PRIMARY KEY, "value" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE sqlite_sequence(name,seq);
CREATE TABLE IF NOT EXISTS "dim_ticket_contact_types" ("dim_ticket_contact_type_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "contact_type" varchar(50) DEFAULT '' NOT NULL, "is_active" boolean DEFAULT 1);
CREATE UNIQUE INDEX "index_dim_ticket_contact_types_on_contact_type" ON "dim_ticket_contact_types" ("contact_type");
CREATE TABLE IF NOT EXISTS "dim_ticket_priorities" ("dim_ticket_priority_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "priority" varchar(50) DEFAULT '' NOT NULL, "is_active" boolean DEFAULT 1);
CREATE UNIQUE INDEX "index_dim_ticket_priorities_on_priority" ON "dim_ticket_priorities" ("priority");
CREATE TABLE IF NOT EXISTS "dim_ticket_urgencies" ("dim_ticket_urgency_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "urgency" varchar(50) DEFAULT '' NOT NULL, "is_active" boolean DEFAULT 1);
CREATE UNIQUE INDEX "index_dim_ticket_urgencies_on_urgency" ON "dim_ticket_urgencies" ("urgency");
CREATE TABLE IF NOT EXISTS "dim_ticket_impacts" ("dim_ticket_impact_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "impact" varchar(50) DEFAULT '' NOT NULL, "is_active" boolean DEFAULT 1);
CREATE UNIQUE INDEX "index_dim_ticket_impacts_on_impact" ON "dim_ticket_impacts" ("impact");
CREATE TABLE IF NOT EXISTS "dim_ticket_categories" ("dim_ticket_category_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "category" varchar(50) DEFAULT '' NOT NULL, "is_active" boolean DEFAULT 1);
CREATE UNIQUE INDEX "index_dim_ticket_categories_on_category" ON "dim_ticket_categories" ("category");
CREATE TABLE IF NOT EXISTS "dim_ticket_symptoms" ("dim_ticket_symptom_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "symptom" varchar(50) DEFAULT '' NOT NULL, "is_active" boolean DEFAULT 1);
CREATE UNIQUE INDEX "index_dim_ticket_symptoms_on_symptom" ON "dim_ticket_symptoms" ("symptom");
CREATE TABLE IF NOT EXISTS "dim_ticket_business_services" ("dim_ticket_business_service_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "business_service" varchar(50) DEFAULT '' NOT NULL, "is_active" boolean DEFAULT 1);
CREATE UNIQUE INDEX "index_dim_ticket_business_services_on_business_service" ON "dim_ticket_business_services" ("business_service");
CREATE TABLE IF NOT EXISTS "dim_ticket_cis" ("dim_ticket_ci_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "ci_name" varchar(100) DEFAULT '' NOT NULL, "is_active" boolean DEFAULT 1);
CREATE UNIQUE INDEX "index_dim_ticket_cis_on_ci_name" ON "dim_ticket_cis" ("ci_name");
CREATE TABLE IF NOT EXISTS "dim_locations" ("dim_location_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "location_name" varchar(50) DEFAULT '' NOT NULL, "is_active" boolean DEFAULT 1);
CREATE UNIQUE INDEX "index_dim_locations_on_location_name" ON "dim_locations" ("location_name");
CREATE TABLE IF NOT EXISTS "dim_user_titles" ("dim_user_title_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "user_title" varchar(100) DEFAULT '' NOT NULL, "is_active" boolean DEFAULT 1);
CREATE UNIQUE INDEX "index_dim_user_titles_on_user_title" ON "dim_user_titles" ("user_title");
CREATE TABLE IF NOT EXISTS "dim_ticket_metrics" ("dim_ticket_metric_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "metric_name" varchar(100) DEFAULT '' NOT NULL, "is_active" boolean DEFAULT 1);
CREATE UNIQUE INDEX "index_dim_ticket_metrics_on_metric_name" ON "dim_ticket_metrics" ("metric_name");
CREATE TABLE IF NOT EXISTS "dim_ticket_close_codes" ("dim_ticket_close_code_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "close_code" varchar(50) DEFAULT '' NOT NULL, "is_active" boolean DEFAULT 1);
CREATE UNIQUE INDEX "index_dim_ticket_close_codes_on_close_code" ON "dim_ticket_close_codes" ("close_code");
CREATE TABLE IF NOT EXISTS "dim_ticket_resolution_methods" ("dim_ticket_resolution_method_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "resolution_method" varchar(50) DEFAULT '' NOT NULL, "is_active" boolean DEFAULT 1);
CREATE UNIQUE INDEX "index_dim_ticket_resolution_methods_on_resolution_method" ON "dim_ticket_resolution_methods" ("resolution_method");
CREATE TABLE IF NOT EXISTS "dim_regions" ("dim_region_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "region_name" varchar(50) DEFAULT '' NOT NULL, "is_active" boolean DEFAULT 1);
CREATE UNIQUE INDEX "index_dim_regions_on_region_name" ON "dim_regions" ("region_name");
CREATE TABLE IF NOT EXISTS "dim_projects" ("dim_project_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "project_name" varchar(100) DEFAULT '' NOT NULL, "is_active" boolean DEFAULT 1);
CREATE UNIQUE INDEX "index_dim_projects_on_project_name" ON "dim_projects" ("project_name");
CREATE TABLE IF NOT EXISTS "dim_clients" ("dim_client_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "client_name" varchar(100) DEFAULT '' NOT NULL, "dim_project_id" integer, "is_active" boolean DEFAULT 1, CONSTRAINT "fk_rails_eca7f268b7"
FOREIGN KEY ("dim_project_id")
  REFERENCES "dim_projects" ("id")
);
CREATE INDEX "index_dim_clients_on_dim_project_id" ON "dim_clients" ("dim_project_id");
CREATE UNIQUE INDEX "index_dim_clients_on_client_name" ON "dim_clients" ("client_name");
CREATE TABLE IF NOT EXISTS "dim_emp_users" ("dim_emp_user_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "emp_id" varchar(50) DEFAULT '' NOT NULL, "login_id" varchar(50) DEFAULT '' NOT NULL, "first_name" varchar(100) DEFAULT '' NOT NULL, "last_name" varchar(100) DEFAULT '' NOT NULL, "hire_date" date NOT NULL, "term_date" date, "supervisor_id" integer, "is_active" boolean DEFAULT 1);
CREATE INDEX "index_dim_emp_users_on_supervisor_id" ON "dim_emp_users" ("supervisor_id");
CREATE UNIQUE INDEX "index_dim_emp_users_on_emp_id" ON "dim_emp_users" ("emp_id");
CREATE UNIQUE INDEX "index_dim_emp_users_on_login_id" ON "dim_emp_users" ("login_id");
CREATE TABLE IF NOT EXISTS "dim_ticket_users" ("dim_ticket_user_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "dim_emp_user_id" integer, "dim_project_id" integer, "ticket_user_id" varchar(50) DEFAULT '' NOT NULL, "start_date" date NOT NULL, "end_date" date, "supervisor_id" integer, "is_active" boolean DEFAULT 1, CONSTRAINT "fk_rails_3c7b2359dc"
FOREIGN KEY ("dim_emp_user_id")
  REFERENCES "dim_emp_users" ("id")
, CONSTRAINT "fk_rails_0577d00b4c"
FOREIGN KEY ("dim_project_id")
  REFERENCES "dim_projects" ("id")
);
CREATE INDEX "index_dim_ticket_users_on_dim_emp_user_id" ON "dim_ticket_users" ("dim_emp_user_id");
CREATE INDEX "index_dim_ticket_users_on_dim_project_id" ON "dim_ticket_users" ("dim_project_id");
CREATE INDEX "index_dim_ticket_users_on_supervisor_id" ON "dim_ticket_users" ("supervisor_id");
CREATE UNIQUE INDEX "index_dim_ticket_users_on_ticket_user_id" ON "dim_ticket_users" ("ticket_user_id");
CREATE TABLE IF NOT EXISTS "dim_groups" ("dim_group_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "group_name" varchar(100) DEFAULT '' NOT NULL, "dim_project_id" integer, "is_active" boolean DEFAULT 1, CONSTRAINT "fk_rails_1349604201"
FOREIGN KEY ("dim_project_id")
  REFERENCES "dim_projects" ("id")
);
CREATE INDEX "index_dim_groups_on_dim_project_id" ON "dim_groups" ("dim_project_id");
CREATE UNIQUE INDEX "index_dim_groups_on_group_name" ON "dim_groups" ("group_name");
CREATE TABLE IF NOT EXISTS "group_member" ("dim_group_id" integer NOT NULL, "dim_ticket_user_id" integer NOT NULL);
CREATE TABLE IF NOT EXISTS "staging_metric_workers" ("staging_metric_worker_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "project_name" varchar, "ticket_number" varchar, "ticket_opened_at" datetime, "ticket_resolved_at" datetime, "ticket_closed_at" datetime, "ticket_state" varchar, "ticket_contact_type" varchar, "ticket_priority" varchar, "ticket_urgency" varchar, "ticket_impact" varchar, "ticket_category" varchar, "ticket_symptom" varchar, "business_service" varchar, "ci_name" varchar, "ticket_opened_by_userid" varchar, "ticket_opened_by_location" varchar, "ticket_caller_title" varchar, "metric_name" varchar, "metric_value" varchar, "metric_started_at" datetime, "metric_ended_at" datetime, "ticket_reassignment_count" integer, "ticket_resolved_by_userid" varchar, "ticket_resolved_by_group" varchar, "ticket_assigned_to_userid" varchar, "ticket_assignment_group" varchar, "ticket_close_code" varchar, "ticket_resolution_method" varchar);
INSERT INTO "schema_migrations" (version) VALUES
('20190520025205'),
('20190520031048'),
('20190520031134'),
('20190520031204'),
('20190520031239'),
('20190520031418'),
('20190520031517'),
('20190520031657'),
('20190520031802'),
('20190520032157'),
('20190520032218'),
('20190520032247'),
('20190520032305'),
('20190520033012'),
('20190520033052'),
('20190520033135'),
('20190520034207'),
('20190520065958'),
('20190520071840'),
('20190520075038'),
('20190520081058');


