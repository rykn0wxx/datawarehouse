# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_20_081058) do

  create_table "dim_clients", primary_key: "dim_client_id", force: :cascade do |t|
    t.string "client_name", limit: 100, default: "", null: false
    t.integer "dim_project_id"
    t.boolean "is_active", default: true
    t.index ["client_name"], name: "index_dim_clients_on_client_name", unique: true
    t.index ["dim_project_id"], name: "index_dim_clients_on_dim_project_id"
  end

  create_table "dim_emp_users", primary_key: "dim_emp_user_id", force: :cascade do |t|
    t.string "emp_id", limit: 50, default: "", null: false
    t.string "login_id", limit: 50, default: "", null: false
    t.string "first_name", limit: 100, default: "", null: false
    t.string "last_name", limit: 100, default: "", null: false
    t.date "hire_date", null: false
    t.date "term_date"
    t.integer "supervisor_id"
    t.boolean "is_active", default: true
    t.index ["emp_id"], name: "index_dim_emp_users_on_emp_id", unique: true
    t.index ["login_id"], name: "index_dim_emp_users_on_login_id", unique: true
    t.index ["supervisor_id"], name: "index_dim_emp_users_on_supervisor_id"
  end

  create_table "dim_groups", primary_key: "dim_group_id", force: :cascade do |t|
    t.string "group_name", limit: 100, default: "", null: false
    t.integer "dim_project_id"
    t.boolean "is_active", default: true
    t.index ["dim_project_id"], name: "index_dim_groups_on_dim_project_id"
    t.index ["group_name"], name: "index_dim_groups_on_group_name", unique: true
  end

  create_table "dim_locations", primary_key: "dim_location_id", force: :cascade do |t|
    t.string "location_name", limit: 50, default: "", null: false
    t.boolean "is_active", default: true
    t.index ["location_name"], name: "index_dim_locations_on_location_name", unique: true
  end

  create_table "dim_projects", primary_key: "dim_project_id", force: :cascade do |t|
    t.string "project_name", limit: 100, default: "", null: false
    t.boolean "is_active", default: true
    t.index ["project_name"], name: "index_dim_projects_on_project_name", unique: true
  end

  create_table "dim_regions", primary_key: "dim_region_id", force: :cascade do |t|
    t.string "region_name", limit: 50, default: "", null: false
    t.boolean "is_active", default: true
    t.index ["region_name"], name: "index_dim_regions_on_region_name", unique: true
  end

  create_table "dim_ticket_business_services", primary_key: "dim_ticket_business_service_id", force: :cascade do |t|
    t.string "business_service", limit: 50, default: "", null: false
    t.boolean "is_active", default: true
    t.index ["business_service"], name: "index_dim_ticket_business_services_on_business_service", unique: true
  end

  create_table "dim_ticket_categories", primary_key: "dim_ticket_category_id", force: :cascade do |t|
    t.string "category", limit: 50, default: "", null: false
    t.boolean "is_active", default: true
    t.index ["category"], name: "index_dim_ticket_categories_on_category", unique: true
  end

  create_table "dim_ticket_cis", primary_key: "dim_ticket_ci_id", force: :cascade do |t|
    t.string "ci_name", limit: 100, default: "", null: false
    t.boolean "is_active", default: true
    t.index ["ci_name"], name: "index_dim_ticket_cis_on_ci_name", unique: true
  end

  create_table "dim_ticket_close_codes", primary_key: "dim_ticket_close_code_id", force: :cascade do |t|
    t.string "close_code", limit: 50, default: "", null: false
    t.boolean "is_active", default: true
    t.index ["close_code"], name: "index_dim_ticket_close_codes_on_close_code", unique: true
  end

  create_table "dim_ticket_contact_types", primary_key: "dim_ticket_contact_type_id", force: :cascade do |t|
    t.string "contact_type", limit: 50, default: "", null: false
    t.boolean "is_active", default: true
    t.index ["contact_type"], name: "index_dim_ticket_contact_types_on_contact_type", unique: true
  end

  create_table "dim_ticket_impacts", primary_key: "dim_ticket_impact_id", force: :cascade do |t|
    t.string "impact", limit: 50, default: "", null: false
    t.boolean "is_active", default: true
    t.index ["impact"], name: "index_dim_ticket_impacts_on_impact", unique: true
  end

  create_table "dim_ticket_metrics", primary_key: "dim_ticket_metric_id", force: :cascade do |t|
    t.string "metric_name", limit: 100, default: "", null: false
    t.boolean "is_active", default: true
    t.index ["metric_name"], name: "index_dim_ticket_metrics_on_metric_name", unique: true
  end

  create_table "dim_ticket_priorities", primary_key: "dim_ticket_priority_id", force: :cascade do |t|
    t.string "priority", limit: 50, default: "", null: false
    t.boolean "is_active", default: true
    t.index ["priority"], name: "index_dim_ticket_priorities_on_priority", unique: true
  end

  create_table "dim_ticket_resolution_methods", primary_key: "dim_ticket_resolution_method_id", force: :cascade do |t|
    t.string "resolution_method", limit: 50, default: "", null: false
    t.boolean "is_active", default: true
    t.index ["resolution_method"], name: "index_dim_ticket_resolution_methods_on_resolution_method", unique: true
  end

  create_table "dim_ticket_symptoms", primary_key: "dim_ticket_symptom_id", force: :cascade do |t|
    t.string "symptom", limit: 50, default: "", null: false
    t.boolean "is_active", default: true
    t.index ["symptom"], name: "index_dim_ticket_symptoms_on_symptom", unique: true
  end

  create_table "dim_ticket_urgencies", primary_key: "dim_ticket_urgency_id", force: :cascade do |t|
    t.string "urgency", limit: 50, default: "", null: false
    t.boolean "is_active", default: true
    t.index ["urgency"], name: "index_dim_ticket_urgencies_on_urgency", unique: true
  end

  create_table "dim_ticket_users", primary_key: "dim_ticket_user_id", force: :cascade do |t|
    t.integer "dim_emp_user_id"
    t.integer "dim_project_id"
    t.string "ticket_user_id", limit: 50, default: "", null: false
    t.date "start_date", null: false
    t.date "end_date"
    t.integer "supervisor_id"
    t.boolean "is_active", default: true
    t.index ["dim_emp_user_id"], name: "index_dim_ticket_users_on_dim_emp_user_id"
    t.index ["dim_project_id"], name: "index_dim_ticket_users_on_dim_project_id"
    t.index ["supervisor_id"], name: "index_dim_ticket_users_on_supervisor_id"
    t.index ["ticket_user_id"], name: "index_dim_ticket_users_on_ticket_user_id", unique: true
  end

  create_table "dim_user_titles", primary_key: "dim_user_title_id", force: :cascade do |t|
    t.string "user_title", limit: 100, default: "", null: false
    t.boolean "is_active", default: true
    t.index ["user_title"], name: "index_dim_user_titles_on_user_title", unique: true
  end

  create_table "group_member", id: false, force: :cascade do |t|
    t.integer "dim_group_id", null: false
    t.integer "dim_ticket_user_id", null: false
  end

  create_table "staging_metric_workers", primary_key: "staging_metric_worker_id", force: :cascade do |t|
    t.string "project_name"
    t.string "ticket_number"
    t.datetime "ticket_opened_at"
    t.datetime "ticket_resolved_at"
    t.datetime "ticket_closed_at"
    t.string "ticket_state"
    t.string "ticket_contact_type"
    t.string "ticket_priority"
    t.string "ticket_urgency"
    t.string "ticket_impact"
    t.string "ticket_category"
    t.string "ticket_symptom"
    t.string "business_service"
    t.string "ci_name"
    t.string "ticket_opened_by_userid"
    t.string "ticket_opened_by_location"
    t.string "ticket_caller_title"
    t.string "metric_name"
    t.string "metric_value"
    t.datetime "metric_started_at"
    t.datetime "metric_ended_at"
    t.integer "ticket_reassignment_count"
    t.string "ticket_resolved_by_userid"
    t.string "ticket_resolved_by_group"
    t.string "ticket_assigned_to_userid"
    t.string "ticket_assignment_group"
    t.string "ticket_close_code"
    t.string "ticket_resolution_method"
  end

end
