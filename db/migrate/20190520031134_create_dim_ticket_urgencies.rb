class CreateDimTicketUrgencies < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_ticket_urgencies do |t|
      t.string :urgency, limit: 50, null: false, default: ''
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_ticket_urgencies, :urgency, unique: true
  end
end
