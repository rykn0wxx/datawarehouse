class CreateDimTicketContactTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_ticket_contact_types do |t|
      t.string :contact_type, limit: 50, null: false, default: ''
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_ticket_contact_types, :contact_type, unique: true
  end
end
