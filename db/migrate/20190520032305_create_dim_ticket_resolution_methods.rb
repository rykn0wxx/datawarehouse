class CreateDimTicketResolutionMethods < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_ticket_resolution_methods do |t|
      t.string :resolution_method, limit: 50, null: false, default: ''
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_ticket_resolution_methods, :resolution_method, unique: true
  end
end
