class CreateDimTicketCis < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_ticket_cis do |t|
      t.string :ci_name, limit: 100, null: false, default: ''
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_ticket_cis, :ci_name, unique: true
  end
end
