class CreateDimTicketPriorities < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_ticket_priorities do |t|
      t.string :priority, limit: 50, null: false, default: ''
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_ticket_priorities, :priority, unique: true
  end
end
