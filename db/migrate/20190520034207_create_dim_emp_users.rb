class CreateDimEmpUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_emp_users do |t|
      t.string :emp_id, limit: 50, null: false, default: ''
      t.string :login_id, limit: 50, null: false, default: ''
      t.string :first_name, limit: 100, null: false, default: ''
      t.string :last_name, limit: 100, null: false, default: ''
      t.date :hire_date, null: false
      t.date :term_date
      t.references :supervisor, index: true
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_emp_users, :emp_id, unique: true
    add_index :dim_emp_users, :login_id, unique: true
  end
end
