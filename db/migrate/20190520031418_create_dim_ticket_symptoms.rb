class CreateDimTicketSymptoms < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_ticket_symptoms do |t|
      t.string :symptom, limit: 50, null: false, default: ''
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_ticket_symptoms, :symptom, unique: true
  end
end
