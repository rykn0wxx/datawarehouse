class CreateDimTicketCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_ticket_categories do |t|
      t.string :category, limit: 50, null: false, default: ''
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_ticket_categories, :category, unique: true
  end
end
