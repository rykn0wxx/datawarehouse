class CreateStagingMetricWorkers < ActiveRecord::Migration[5.2]
  def change
    create_table :staging_metric_workers do |t|
      t.string :project_name
      t.string :ticket_number
      t.datetime :ticket_opened_at
      t.datetime :ticket_resolved_at
      t.datetime :ticket_closed_at
      t.string :ticket_state
      t.string :ticket_contact_type
      t.string :ticket_priority
      t.string :ticket_urgency
      t.string :ticket_impact
      t.string :ticket_category
      t.string :ticket_symptom
      t.string :business_service
      t.string :ci_name
      t.string :ticket_opened_by_userid
      t.string :ticket_opened_by_location
      t.string :ticket_caller_title
      t.string :metric_name
      t.string :metric_value
      t.datetime :metric_started_at
      t.datetime :metric_ended_at
      t.integer :ticket_reassignment_count
      t.string :ticket_resolved_by_userid
      t.string :ticket_resolved_by_group
      t.string :ticket_assigned_to_userid
      t.string :ticket_assignment_group
      t.string :ticket_close_code
      t.string :ticket_resolution_method

      # t.timestamps
    end
  end
end
