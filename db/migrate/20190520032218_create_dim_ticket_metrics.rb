class CreateDimTicketMetrics < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_ticket_metrics do |t|
      t.string :metric_name, limit: 100, null: false, default: ''
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_ticket_metrics, :metric_name, unique: true
  end
end
