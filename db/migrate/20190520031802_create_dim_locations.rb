class CreateDimLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_locations do |t|
      t.string :location_name, limit: 50, null: false, default: ''
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_locations, :location_name, unique: true
  end
end
