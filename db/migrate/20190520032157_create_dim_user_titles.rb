class CreateDimUserTitles < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_user_titles do |t|
      t.string :user_title, limit: 100, null: false, default: ''
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_user_titles, :user_title, unique: true
  end
end
