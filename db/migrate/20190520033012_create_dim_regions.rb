class CreateDimRegions < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_regions do |t|
      t.string :region_name, limit: 50, null: false, default: ''
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_regions, :region_name, unique: true
  end
end
