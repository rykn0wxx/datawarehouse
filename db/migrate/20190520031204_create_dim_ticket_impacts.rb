class CreateDimTicketImpacts < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_ticket_impacts do |t|
      t.string :impact, limit: 50, null: false, default: ''
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_ticket_impacts, :impact, unique: true
  end
end
