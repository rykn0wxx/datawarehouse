class CreateDimTicketUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_ticket_users do |t|
      t.references :dim_emp_user, foreign_key: true
      t.references :dim_project, foreign_key: true
      t.string :ticket_user_id, limit: 50, null: false, default: ''
      t.date :start_date, null: false
      t.date :end_date
      t.references :supervisor, index: true
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_ticket_users, :ticket_user_id, unique: true
  end
end
