class CreateJoinTableGroupMember < ActiveRecord::Migration[5.2]
  def change
    create_join_table :dim_groups, :dim_ticket_users, :table_name => :group_member do |t|
      # t.index [:dim_group_id, :dim_ticket_user_id]
      # t.index [:dim_ticket_user_id, :dim_group_id]
    end
  end
end
