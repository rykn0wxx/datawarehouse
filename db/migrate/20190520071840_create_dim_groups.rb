class CreateDimGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_groups do |t|
      t.string :group_name, limit: 100, null: false, default: ''
      t.references :dim_project, foreign_key: true
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_groups, :group_name, unique: true
  end
end
