class CreateDimTicketCloseCodes < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_ticket_close_codes do |t|
      t.string :close_code, limit: 50, null: false, default: ''
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_ticket_close_codes, :close_code, unique: true
  end
end
