class CreateDimTicketBusinessServices < ActiveRecord::Migration[5.2]
  def change
    create_table :dim_ticket_business_services do |t|
      t.string :business_service, limit: 50, null: false, default: ''
      t.boolean :is_active, default: true

      # t.timestamps
    end
    add_index :dim_ticket_business_services, :business_service, unique: true
  end
end
