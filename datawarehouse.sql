USE [master]
GO

/******	Object: Database [Liquid] Start	******/

/******	Drop Database if Exists	******/
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'Liquid')
	DROP DATABASE [Liquid]
	GO

/******	Create Database	******/
CREATE DATABASE [Liquid]
GO

/******	Object: Database [Liquid] End	******/

/******	Object: Tables for Liquid Database Start	******/

USE [Liquid]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_states]') AND type in (N'U'))
  CREATE TABLE "dim_ticket_states" (
    "dim_ticket_state_id" INT NOT NULL IDENTITY(1, 1),
    "ticket_state" varchar(50) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_ticket_states" PRIMARY KEY ("dim_ticket_state_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_ticket_states_on_ticket_state" ON "dim_ticket_states" ("ticket_state")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_contact_types]') AND type in (N'U'))
  CREATE TABLE "dim_ticket_contact_types" (
    "dim_ticket_contact_type_id" INT NOT NULL IDENTITY(1, 1),
    "contact_type" varchar(50) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_ticket_contact_types" PRIMARY KEY ("dim_ticket_contact_type_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_ticket_contact_types_on_contact_type" ON "dim_ticket_contact_types" ("contact_type")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_priorities]') AND type in (N'U'))
  CREATE TABLE "dim_ticket_priorities" (
    "dim_ticket_priority_id" INT NOT NULL IDENTITY(1, 1),
    "priority" varchar(50) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_ticket_priorities" PRIMARY KEY ("dim_ticket_priority_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_ticket_priorities_on_priority" ON "dim_ticket_priorities" ("priority")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_urgencies]') AND type in (N'U'))
  CREATE TABLE "dim_ticket_urgencies" (
    "dim_ticket_urgency_id" INT NOT NULL IDENTITY(1, 1),
    "urgency" varchar(50) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_ticket_urgencies" PRIMARY KEY ("dim_ticket_urgency_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_ticket_urgencies_on_urgency" ON "dim_ticket_urgencies" ("urgency")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_impacts]') AND type in (N'U'))
  CREATE TABLE "dim_ticket_impacts" (
    "dim_ticket_impact_id" INT NOT NULL IDENTITY(1, 1),
    "impact" varchar(50) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_ticket_impacts" PRIMARY KEY ("dim_ticket_impact_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_ticket_impacts_on_impact" ON "dim_ticket_impacts" ("impact")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_categories]') AND type in (N'U'))
  CREATE TABLE "dim_ticket_categories" (
    "dim_ticket_category_id" INT NOT NULL IDENTITY(1, 1),
    "category" varchar(50) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_ticket_categories" PRIMARY KEY ("dim_ticket_category_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_ticket_categories_on_category" ON "dim_ticket_categories" ("category")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_symptoms]') AND type in (N'U'))
  CREATE TABLE "dim_ticket_symptoms" (
    "dim_ticket_symptom_id" INT NOT NULL IDENTITY(1, 1),
    "symptom" varchar(50) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_ticket_symptoms" PRIMARY KEY ("dim_ticket_symptom_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_ticket_symptoms_on_symptom" ON "dim_ticket_symptoms" ("symptom")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_business_services]') AND type in (N'U'))
  CREATE TABLE "dim_ticket_business_services" (
    "dim_ticket_business_service_id" INT NOT NULL IDENTITY(1, 1),
    "business_service" varchar(50) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_ticket_business_services" PRIMARY KEY ("dim_ticket_business_service_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_ticket_business_services_on_business_service" ON "dim_ticket_business_services" ("business_service")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_cis]') AND type in (N'U'))
  CREATE TABLE "dim_ticket_cis" (
    "dim_ticket_ci_id" INT NOT NULL IDENTITY(1, 1),
    "ci_name" varchar(100) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_ticket_cis" PRIMARY KEY ("dim_ticket_ci_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_ticket_cis_on_ci_name" ON "dim_ticket_cis" ("ci_name")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_locations]') AND type in (N'U'))
  CREATE TABLE "dim_locations" (
    "dim_location_id" INT NOT NULL IDENTITY(1, 1),
    "location_name" varchar(50) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_locations" PRIMARY KEY ("dim_location_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_locations_on_location_name" ON "dim_locations" ("location_name")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_user_titles]') AND type in (N'U'))
  CREATE TABLE "dim_user_titles" (
    "dim_user_title_id" INT NOT NULL IDENTITY(1, 1),
    "user_title" varchar(100) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_user_titles" PRIMARY KEY ("dim_user_title_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_user_titles_on_user_title" ON "dim_user_titles" ("user_title")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_metrics]') AND type in (N'U'))
  CREATE TABLE "dim_ticket_metrics" (
    "dim_ticket_metric_id" INT NOT NULL IDENTITY(1, 1),
    "metric_name" varchar(100) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_ticket_metrics" PRIMARY KEY ("dim_ticket_metric_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_ticket_metrics_on_metric_name" ON "dim_ticket_metrics" ("metric_name")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_close_codes]') AND type in (N'U'))
  CREATE TABLE "dim_ticket_close_codes" (
    "dim_ticket_close_code_id" INT NOT NULL IDENTITY(1, 1),
    "close_code" varchar(50) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_ticket_close_codes" PRIMARY KEY ("dim_ticket_close_code_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_ticket_close_codes_on_close_code" ON "dim_ticket_close_codes" ("close_code")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_resolution_methods]') AND type in (N'U'))
  CREATE TABLE "dim_ticket_resolution_methods" (
    "dim_ticket_resolution_method_id" INT NOT NULL IDENTITY(1, 1),
    "resolution_method" varchar(50) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_ticket_resolution_methods" PRIMARY KEY ("dim_ticket_resolution_method_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_ticket_resolution_methods_on_resolution_method" ON "dim_ticket_resolution_methods" ("resolution_method")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_regions]') AND type in (N'U'))
  CREATE TABLE "dim_regions" (
    "dim_region_id" INT NOT NULL IDENTITY(1, 1),
    "region_name" varchar(50) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_regions" PRIMARY KEY ("dim_region_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_regions_on_region_name" ON "dim_regions" ("region_name")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_projects]') AND type in (N'U'))
  CREATE TABLE "dim_projects" (
    "dim_project_id" INT NOT NULL IDENTITY(1, 1),
    "project_name" varchar(100) DEFAULT '' NOT NULL,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_projects" PRIMARY KEY ("dim_project_id")
  )
GO
CREATE UNIQUE INDEX "index_dim_projects_on_project_name" ON "dim_projects" ("project_name")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_clients]') AND type in (N'U'))
  CREATE TABLE "dim_clients" (
    "dim_client_id" INT NOT NULL IDENTITY(1, 1),
    "client_name" varchar(100) DEFAULT '' NOT NULL,
    "dim_project_id" integer,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_clients" PRIMARY KEY ("dim_client_id"),
    CONSTRAINT "fk_dim_clients_dim_project_id"
      FOREIGN KEY ("dim_project_id")
      REFERENCES "dim_projects" ("dim_project_id")
  )
GO
CREATE INDEX "index_dim_clients_on_dim_project_id" ON "dim_clients" ("dim_project_id")
GO
CREATE UNIQUE INDEX "index_dim_clients_on_client_name" ON "dim_clients" ("client_name")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_emp_users]') AND type in (N'U'))
  CREATE TABLE "dim_emp_users" (
    "dim_emp_user_id" INT NOT NULL IDENTITY(1, 1),
    "emp_id" varchar(50) DEFAULT '' NOT NULL,
    "login_id" varchar(50) DEFAULT '' NOT NULL,
    "first_name" varchar(100) DEFAULT '' NOT NULL,
    "last_name" varchar(100) DEFAULT '' NOT NULL,
    "hire_date" date NOT NULL,
    "term_date" date,
    "supervisor_id" integer,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_emp_users" PRIMARY KEY ("dim_emp_user_id")
  )
GO
CREATE INDEX "index_dim_emp_users_on_supervisor_id" ON "dim_emp_users" ("supervisor_id")
GO
CREATE UNIQUE INDEX "index_dim_emp_users_on_emp_id_login_id" ON "dim_emp_users" ("emp_id", "login_id")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_ticket_users]') AND type in (N'U'))
  CREATE TABLE "dim_ticket_users" (
    "dim_ticket_user_id" INT NOT NULL IDENTITY(1, 1),
    "dim_emp_user_id" integer,
    "dim_project_id" integer,
    "ticket_user_id" varchar(50) DEFAULT '' NOT NULL,
    "start_date" date NOT NULL,
    "end_date" date,
    "supervisor_id" integer,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_ticket_users" PRIMARY KEY ("dim_ticket_user_id"),
    CONSTRAINT "fk_dim_ticket_users_dim_emp_user_id"
      FOREIGN KEY ("dim_emp_user_id")
      REFERENCES "dim_emp_users" ("dim_emp_user_id"),
    CONSTRAINT "fk_dim_ticket_users_dim_project_id"
      FOREIGN KEY ("dim_project_id")
      REFERENCES "dim_projects" ("dim_project_id")
  )
GO
CREATE INDEX "index_dim_ticket_users_on_dim_emp_user_id" ON "dim_ticket_users" ("dim_emp_user_id")
GO
CREATE INDEX "index_dim_ticket_users_on_dim_project_id" ON "dim_ticket_users" ("dim_project_id")
GO
CREATE INDEX "index_dim_ticket_users_on_supervisor_id" ON "dim_ticket_users" ("supervisor_id")
GO
CREATE UNIQUE INDEX "index_dim_ticket_users_on_ticket_user_id" ON "dim_ticket_users" ("ticket_user_id", "dim_emp_user_id", "dim_project_id")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_groups]') AND type in (N'U'))
  CREATE TABLE "dim_groups" (
    "dim_group_id" INT NOT NULL IDENTITY(1, 1),
    "group_name" varchar(100) DEFAULT '' NOT NULL,
    "dim_project_id" integer,
    "is_active" BIT NOT NULL DEFAULT 1,
    CONSTRAINT "PK_dim_groups" PRIMARY KEY ("dim_group_id"),
    CONSTRAINT "fk_dim_groups_dim_project_id"
      FOREIGN KEY ("dim_project_id")
      REFERENCES "dim_projects" ("dim_project_id")
  )
GO
CREATE INDEX "index_dim_groups_on_dim_project_id" ON "dim_groups" ("dim_project_id")
GO
CREATE UNIQUE INDEX "index_dim_groups_on_group_name" ON "dim_groups" ("group_name")
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[group_member]') AND type in (N'U'))
  CREATE TABLE "group_member" (
    "dim_group_id" integer NOT NULL,
    "dim_ticket_user_id" integer NOT NULL,
    CONSTRAINT "fk_group_member_dim_group_id"
      FOREIGN KEY ("dim_group_id")
      REFERENCES "dim_groups" ("dim_group_id"),
    CONSTRAINT "fk_group_member_dim_ticket_user_id"
      FOREIGN KEY ("dim_ticket_user_id")
      REFERENCES "dim_ticket_users" ("dim_ticket_user_id")
  )
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[staging_metric_workers]') AND type in (N'U'))
  CREATE TABLE "staging_metric_workers" (
    "staging_metric_worker_id" INT NOT NULL IDENTITY(1, 1),
    "project_name" varchar,
    "ticket_number" varchar,
    "ticket_opened_at" datetime,
    "ticket_resolved_at" datetime,
    "ticket_closed_at" datetime,
    "ticket_state" varchar,
    "ticket_contact_type" varchar,
    "ticket_priority" varchar,
    "ticket_urgency" varchar,
    "ticket_impact" varchar,
    "ticket_category" varchar,
    "ticket_symptom" varchar,
    "business_service" varchar,
    "ci_name" varchar,
    "ticket_opened_by_userid" varchar,
    "ticket_opened_by_location" varchar,
    "ticket_caller_title" varchar,
    "metric_name" varchar,
    "metric_value" varchar,
    "metric_started_at" datetime,
    "metric_ended_at" datetime,
    "ticket_reassignment_count" integer,
    "ticket_resolved_by_userid" varchar,
    "ticket_resolved_by_group" varchar,
    "ticket_assigned_to_userid" varchar,
    "ticket_assignment_group" varchar,
    "ticket_close_code" varchar,
    "ticket_resolution_method" varchar,
    CONSTRAINT "PK_staging_metric_workers" PRIMARY KEY ("staging_metric_worker_id"),
  )
GO
