USE [Liquid]
GO
/****** Object:  Table [dbo].[dim_clients]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_clients](
	[dim_client_id] [int] IDENTITY(1,1) NOT NULL,
	[client_name] [varchar](100) NOT NULL,
	[dim_project_id] [int] NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_clients] PRIMARY KEY CLUSTERED 
(
	[dim_client_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_emp_users]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_emp_users](
	[dim_emp_user_id] [int] IDENTITY(1,1) NOT NULL,
	[emp_id] [varchar](50) NOT NULL,
	[login_id] [varchar](50) NOT NULL,
	[first_name] [varchar](100) NOT NULL,
	[last_name] [varchar](100) NOT NULL,
	[hire_date] [date] NOT NULL,
	[term_date] [date] NULL,
	[supervisor_id] [int] NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_emp_users] PRIMARY KEY CLUSTERED 
(
	[dim_emp_user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_locations]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_locations](
	[dim_location_id] [int] IDENTITY(1,1) NOT NULL,
	[location_name] [varchar](50) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_locations] PRIMARY KEY CLUSTERED 
(
	[dim_location_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_projects]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_projects](
	[dim_project_id] [int] IDENTITY(1,1) NOT NULL,
	[project_name] [varchar](100) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_projects] PRIMARY KEY CLUSTERED 
(
	[dim_project_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_regions]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_regions](
	[dim_region_id] [int] IDENTITY(1,1) NOT NULL,
	[region_name] [varchar](50) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_regions] PRIMARY KEY CLUSTERED 
(
	[dim_region_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_ticket_business_services]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_ticket_business_services](
	[dim_ticket_business_service_id] [int] IDENTITY(1,1) NOT NULL,
	[business_service] [varchar](50) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_ticket_business_services] PRIMARY KEY CLUSTERED 
(
	[dim_ticket_business_service_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_ticket_categories]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_ticket_categories](
	[dim_ticket_category_id] [int] IDENTITY(1,1) NOT NULL,
	[category] [varchar](50) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_ticket_categories] PRIMARY KEY CLUSTERED 
(
	[dim_ticket_category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_ticket_cis]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_ticket_cis](
	[dim_ticket_ci_id] [int] IDENTITY(1,1) NOT NULL,
	[ci_name] [varchar](100) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_ticket_cis] PRIMARY KEY CLUSTERED 
(
	[dim_ticket_ci_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_ticket_close_codes]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_ticket_close_codes](
	[dim_ticket_close_code_id] [int] IDENTITY(1,1) NOT NULL,
	[close_code] [varchar](50) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_ticket_close_codes] PRIMARY KEY CLUSTERED 
(
	[dim_ticket_close_code_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_ticket_contact_types]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_ticket_contact_types](
	[dim_ticket_contact_type_id] [int] IDENTITY(1,1) NOT NULL,
	[contact_type] [varchar](50) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_ticket_contact_types] PRIMARY KEY CLUSTERED 
(
	[dim_ticket_contact_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_ticket_impacts]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_ticket_impacts](
	[dim_ticket_impact_id] [int] IDENTITY(1,1) NOT NULL,
	[impact] [varchar](50) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_ticket_impacts] PRIMARY KEY CLUSTERED 
(
	[dim_ticket_impact_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_ticket_metrics]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_ticket_metrics](
	[dim_ticket_metric_id] [int] IDENTITY(1,1) NOT NULL,
	[metric_name] [varchar](100) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_ticket_metrics] PRIMARY KEY CLUSTERED 
(
	[dim_ticket_metric_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_ticket_priorities]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_ticket_priorities](
	[dim_ticket_priority_id] [int] IDENTITY(1,1) NOT NULL,
	[priority] [varchar](50) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_ticket_priorities] PRIMARY KEY CLUSTERED 
(
	[dim_ticket_priority_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_ticket_resolution_methods]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_ticket_resolution_methods](
	[dim_ticket_resolution_method_id] [int] IDENTITY(1,1) NOT NULL,
	[resolution_method] [varchar](50) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_ticket_resolution_methods] PRIMARY KEY CLUSTERED 
(
	[dim_ticket_resolution_method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_ticket_symptoms]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_ticket_symptoms](
	[dim_ticket_symptom_id] [int] IDENTITY(1,1) NOT NULL,
	[symptom] [varchar](50) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_ticket_symptoms] PRIMARY KEY CLUSTERED 
(
	[dim_ticket_symptom_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_ticket_urgencies]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_ticket_urgencies](
	[dim_ticket_urgency_id] [int] IDENTITY(1,1) NOT NULL,
	[urgency] [varchar](50) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_ticket_urgencies] PRIMARY KEY CLUSTERED 
(
	[dim_ticket_urgency_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dim_user_titles]    Script Date: 5/20/2019 12:52:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dim_user_titles](
	[dim_user_title_id] [int] IDENTITY(1,1) NOT NULL,
	[user_title] [varchar](100) NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_dim_user_titles] PRIMARY KEY CLUSTERED 
(
	[dim_user_title_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[dim_clients] ADD  DEFAULT ('') FOR [client_name]
GO
ALTER TABLE [dbo].[dim_clients] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_emp_users] ADD  DEFAULT ('') FOR [emp_id]
GO
ALTER TABLE [dbo].[dim_emp_users] ADD  DEFAULT ('') FOR [login_id]
GO
ALTER TABLE [dbo].[dim_emp_users] ADD  DEFAULT ('') FOR [first_name]
GO
ALTER TABLE [dbo].[dim_emp_users] ADD  DEFAULT ('') FOR [last_name]
GO
ALTER TABLE [dbo].[dim_emp_users] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_locations] ADD  DEFAULT ('') FOR [location_name]
GO
ALTER TABLE [dbo].[dim_locations] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_projects] ADD  DEFAULT ('') FOR [project_name]
GO
ALTER TABLE [dbo].[dim_projects] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_regions] ADD  DEFAULT ('') FOR [region_name]
GO
ALTER TABLE [dbo].[dim_regions] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_ticket_business_services] ADD  DEFAULT ('') FOR [business_service]
GO
ALTER TABLE [dbo].[dim_ticket_business_services] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_ticket_categories] ADD  DEFAULT ('') FOR [category]
GO
ALTER TABLE [dbo].[dim_ticket_categories] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_ticket_cis] ADD  DEFAULT ('') FOR [ci_name]
GO
ALTER TABLE [dbo].[dim_ticket_cis] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_ticket_close_codes] ADD  DEFAULT ('') FOR [close_code]
GO
ALTER TABLE [dbo].[dim_ticket_close_codes] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_ticket_contact_types] ADD  DEFAULT ('') FOR [contact_type]
GO
ALTER TABLE [dbo].[dim_ticket_contact_types] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_ticket_impacts] ADD  DEFAULT ('') FOR [impact]
GO
ALTER TABLE [dbo].[dim_ticket_impacts] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_ticket_metrics] ADD  DEFAULT ('') FOR [metric_name]
GO
ALTER TABLE [dbo].[dim_ticket_metrics] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_ticket_priorities] ADD  DEFAULT ('') FOR [priority]
GO
ALTER TABLE [dbo].[dim_ticket_priorities] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_ticket_resolution_methods] ADD  DEFAULT ('') FOR [resolution_method]
GO
ALTER TABLE [dbo].[dim_ticket_resolution_methods] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_ticket_symptoms] ADD  DEFAULT ('') FOR [symptom]
GO
ALTER TABLE [dbo].[dim_ticket_symptoms] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_ticket_urgencies] ADD  DEFAULT ('') FOR [urgency]
GO
ALTER TABLE [dbo].[dim_ticket_urgencies] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_user_titles] ADD  DEFAULT ('') FOR [user_title]
GO
ALTER TABLE [dbo].[dim_user_titles] ADD  DEFAULT ((1)) FOR [is_active]
GO
ALTER TABLE [dbo].[dim_clients]  WITH CHECK ADD  CONSTRAINT [fk_dim_clients_dim_project_id] FOREIGN KEY([dim_project_id])
REFERENCES [dbo].[dim_projects] ([dim_project_id])
GO
ALTER TABLE [dbo].[dim_clients] CHECK CONSTRAINT [fk_dim_clients_dim_project_id]
GO
