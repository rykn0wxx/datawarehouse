class DimTicketUser < ApplicationRecord
  belongs_to :dim_emp_user
  belongs_to :dim_project
  belongs_to :supervisor
end
