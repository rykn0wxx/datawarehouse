USE [Liquid]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_date]') AND type in (N'U'))
  DROP TABLE [dbo].[dim_date]
  GO

USE [Liquid]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dim_date]') AND type in (N'U'))
  CREATE TABLE [dbo].[dim_date] (
    "dim_date_id" INT NOT NULL IDENTITY(41640, 1),
    "full_date" DATE,
    "day_of_month" SMALLINT,
    "day_name" VARCHAR (10),
    "day_of_week" SMALLINT,
    "month_of_year" SMALLINT,
    "month_name" VARCHAR (10),
    "month_quarter" SMALLINT,
    "year_number" SMALLINT,
    "year_name" VARCHAR (10),
    "month_year" VARCHAR (10),
    "date_key" VARCHAR (10),
    "firstday_of_month" DATE,
    "lastday_of_month" DATE,
    "is_weekend" BIT,
    "report_week" DATE,
    CONSTRAINT "PK_dim_date" PRIMARY KEY ("dim_date_id")
  )
GO

/********************************************************************************************/
-- Start populating dim date
DECLARE @InitDateKey INT = '41640'
DECLARE @StartDate DATETIME = '01/01/2014'
DECLARE @EndDate DATETIME = '01/01/2030'

DECLARE @CurrentDate AS DATETIME = @StartDate

WHILE @CurrentDate < @EndDate
BEGIN
  INSERT INTO dbo.dim_date
  SELECT
    @CurrentDate AS full_date,
    DATEPART(DD, @CurrentDate) AS day_of_month,
    DATENAME(DW, @CurrentDate) AS day_name,
    DATEPART(DW, DATEADD(D, 1, @CurrentDate)) AS day_of_week,
    DATEPART(M, @CurrentDate) AS month_of_year,
    DATENAME(M, @CurrentDate) AS month_name,
    DATEPART(QQ, @CurrentDate) AS month_of_quarter,
    DATEPART(YEAR, @CurrentDate) AS year_number,
    'Yr ' + CONVERT(varchar, DATEPART(YEAR, @CurrentDate)) as year_name,
    LEFT(DATENAME(MM, @CurrentDate), 3) + '-' + CONVERT(varchar, DATEPART(YEAR, @CurrentDate)) as month_year,
    CONVERT (char(8), @CurrentDate, 112) AS date_key,
    CONVERT(DATETIME, CONVERT(DATE, DATEADD(DD, - (DATEPART(DD, @CurrentDate) - 1), @CurrentDate))) AS firstday_of_month,
    CONVERT(DATETIME, CONVERT(DATE, DATEADD(DD, - (DATEPART(DD, (DATEADD(MM, 1, @CurrentDate)))), DATEADD(MM, 1, @CurrentDate)))) AS lastday_of_month,
    CASE DATEPART(DW, @CurrentDate)
			WHEN 1 THEN 1
			WHEN 2 THEN 0
			WHEN 3 THEN 0
			WHEN 4 THEN 0
			WHEN 5 THEN 0
			WHEN 6 THEN 0
			WHEN 7 THEN 1
    END AS is_weekend,
    DATEADD(DD, -(DATEPART(DW, @CurrentDate)), @CurrentDate) AS report_week

  SET @CurrentDate = DATEADD(DD, 1, @CurrentDate)
END

/********************************************************************************************/
